package ee.zalizko.george.finestmedia.model.web;

/**
 * @author Georgii Zalizko
 *
 */
public enum ResponseStatus {
	OK, ERROR
}
