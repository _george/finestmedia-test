package ee.zalizko.george.finestmedia.model.web;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Georgii Zalizko
 * 
 */
@Getter
@Setter
public class Currency implements Comparable<Currency> {

	public String code;

	public String name;

	@Override
	public int compareTo(Currency c) {
		return getName().compareToIgnoreCase(c.getName());
	}
}
