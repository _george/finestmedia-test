package ee.zalizko.george.finestmedia.model;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Georgii Zalizko
 * 
 */
@Getter
@Setter
@NoArgsConstructor
public class ExchangeRate {

	public Date date;

	public String currency;

	public float rate;
}
