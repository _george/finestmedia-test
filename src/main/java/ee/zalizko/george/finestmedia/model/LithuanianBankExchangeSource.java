package ee.zalizko.george.finestmedia.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import ee.zalizko.george.finestmedia.exception.ExchangeSourceIOException;
import ee.zalizko.george.finestmedia.model.web.Currency;

/**
 * @author Georgii Zalizko
 *
 */
public class LithuanianBankExchangeSource implements ExchangeSource {

	@SuppressWarnings("unused")
	private static final String URL_LITHUANIAN_BANK = "http://webservices.lb.lt/ExchangeRates/ExchangeRates.asmx/getExchangeRatesByDate?Date=%DATE%";

	@Override
	public Set<Currency> getCurrencyList(Date date) throws ExchangeSourceIOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, ExchangeRate> getExchangeCourseList(Date date) throws ExchangeSourceIOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExchangeSourceName() {
		// TODO Auto-generated method stub
		return null;
	}

}
