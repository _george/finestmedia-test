package ee.zalizko.george.finestmedia.model;

import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.oxm.Unmarshaller;
import org.springframework.stereotype.Component;

import ee.zalizko.george.finestmedia.exception.ExchangeSourceIOException;
import ee.zalizko.george.finestmedia.model.external.Report;
import ee.zalizko.george.finestmedia.model.web.Currency;

/**
 * @author Georgii Zalizko
 *
 */
@Component
@Slf4j
public class EstonianBankExchangeSource implements ExchangeSource {

	@Autowired
	Unmarshaller unmarshaller;

	@Autowired
	MessageSource messageSource;

	private static final String URL_ESTONIAN_BANK = "http://statistika.eestipank.ee/Reports?type=curd&format=xml&date1=%DATE%&lng=est&print=off";

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public Set<Currency> getCurrencyList(Date date) throws ExchangeSourceIOException {
		Report report = getReport(date);

		Set<Currency> currencies = new TreeSet<>();
		for (ee.zalizko.george.finestmedia.model.external.Report.Body.Currencies.Currency c : report.getBody().getCurrencies()
				.getCurrency()) {
			Currency curr = new Currency();
			curr.setCode(c.getName());
			curr.setName(c.getText());
			currencies.add(curr);
		}

		return currencies;
	}

	@Override
	public Map<String, ExchangeRate> getExchangeCourseList(Date date) throws ExchangeSourceIOException {
		Report report = getReport(date);

		Map<String, ExchangeRate> courses = new HashMap<String, ExchangeRate>();

		for (ee.zalizko.george.finestmedia.model.external.Report.Body.Currencies.Currency c : report.getBody().getCurrencies()
				.getCurrency()) {
			ExchangeRate course = new ExchangeRate();
			course.setCurrency(c.getName());
			course.setDate(date);

			DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols();
			unusualSymbols.setGroupingSeparator(' ');
			unusualSymbols.setDecimalSeparator(',');

			DecimalFormat df = new DecimalFormat("###,###.##########", unusualSymbols);

			Number num = 0;
			try {
				num = df.parse(c.getRate());
			} catch (ParseException e) {
			}

			course.setRate(num.floatValue());
			courses.put(c.getName(), course);
		}
		return courses;
	}

	@Override
	public String getExchangeSourceName() {
		return messageSource.getMessage("exchange.source.estonian.bank", null, LocaleContextHolder.getLocale());
	}

	private Report getReport(Date date) throws ExchangeSourceIOException {
		try {
			String preparedUrl = prepareURL(date);
			log.debug("get data from: {}", preparedUrl);

			URL url = new URL(preparedUrl);
			URLConnection conn = url.openConnection();

			Source source = new StreamSource(conn.getInputStream());
			Report report = (Report) unmarshaller.unmarshal(source);

			return report;
		} catch (Exception e) {
			log.error("Error while trying to get report from {}", getExchangeSourceName(), e);
			throw new ExchangeSourceIOException();
		}
	}

	private String prepareURL(Date date) {
		return URL_ESTONIAN_BANK.replace("%DATE%", sdf.format(date));
	}

}
