package ee.zalizko.george.finestmedia.model.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Georgii Zalizko
 * 
 */
@Getter
@Setter
@AllArgsConstructor
public class ErrorMessage {

	private String fieldName;

	private String message;
}
