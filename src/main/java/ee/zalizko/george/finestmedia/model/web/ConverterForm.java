package ee.zalizko.george.finestmedia.model.web;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * @author Georgii Zalizko
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class ConverterForm {

	@NotNull
	BigDecimal amount;

	@NotEmpty
	String currencyFrom;

	@NotEmpty
	String currencyTo;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE, pattern = "dd.MM.yyyy")
	Date date;
}
