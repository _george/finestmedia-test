package ee.zalizko.george.finestmedia.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import ee.zalizko.george.finestmedia.exception.ExchangeSourceIOException;
import ee.zalizko.george.finestmedia.model.web.Currency;

/**
 * @author Georgii Zalizko
 *
 */
public interface ExchangeSource {

	public Map<String, ExchangeRate> getExchangeCourseList(Date date) throws ExchangeSourceIOException;

	public Set<Currency> getCurrencyList(Date date) throws ExchangeSourceIOException;

	public String getExchangeSourceName();
}
