package ee.zalizko.george.finestmedia.model.web;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Georgii Zalizko
 * 
 */
@Getter
@Setter
@NoArgsConstructor
public class ConverterResponse {

	ResponseStatus status;

	List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

	String result;
}
