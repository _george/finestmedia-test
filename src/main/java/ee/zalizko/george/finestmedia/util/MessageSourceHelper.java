package ee.zalizko.george.finestmedia.util;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

/**
 * @author Georgii Zalizko
 * 
 */
@Component
@Slf4j
public class MessageSourceHelper {

	@Autowired
	MessageSource messageSource;

	public String getFieldErrorMessage(FieldError fieldError) {

		String result = fieldError.getField() + " " + fieldError.getDefaultMessage();

		for (String code : fieldError.getCodes()) {
			try {
				return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
			} catch (NoSuchMessageException e) {
				log.error("No message found for code: {} (args: {}, locale: {})", code, null, LocaleContextHolder.getLocale());
			}
		}
		return result;
	}

	public String getMessage(String code, Object[] args) {
		try {
			return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
		} catch (NoSuchMessageException e) {
			log.error("No message found for code: {} (args: {}, locale: {})", code, null, LocaleContextHolder.getLocale());
			return code;
		}
	}
}
