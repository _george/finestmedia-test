package ee.zalizko.george.finestmedia.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ee.zalizko.george.finestmedia.model.web.ConverterForm;

/**
 * @author Georgii Zalizko
 *
 */
public class ConverterFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ConverterForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

	}

}
