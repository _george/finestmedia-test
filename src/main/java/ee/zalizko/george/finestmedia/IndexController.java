package ee.zalizko.george.finestmedia;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ee.zalizko.george.finestmedia.exception.ExchangeSourceIOException;
import ee.zalizko.george.finestmedia.exception.NoCourceException;
import ee.zalizko.george.finestmedia.model.ExchangeRate;
import ee.zalizko.george.finestmedia.model.ExchangeSource;
import ee.zalizko.george.finestmedia.model.web.ConverterForm;
import ee.zalizko.george.finestmedia.model.web.ConverterResponse;
import ee.zalizko.george.finestmedia.model.web.Currency;
import ee.zalizko.george.finestmedia.model.web.ErrorMessage;
import ee.zalizko.george.finestmedia.model.web.ResponseStatus;
import ee.zalizko.george.finestmedia.util.MessageSourceHelper;
import ee.zalizko.george.finestmedia.validator.ConverterFormValidator;

/**
 * @author Georgii Zalizko
 * 
 */
@Controller
@Slf4j
public class IndexController {

	@Autowired
	MessageSourceHelper messageSourceHelper;

	@Autowired
	ApplicationContext applicationContext;

	private Map<String, ExchangeSource> exchangeSources;

	public void attachCurrenciesToModel(Model model) {
		Calendar cal = Calendar.getInstance();
		cal.set(2010, 11, 31);
		Date date = cal.getTime();

		log.debug("get currencies list for date: {}", date);

		Set<Currency> currencies = new TreeSet<Currency>();

		for (Entry<String, ExchangeSource> exchangeSourceEntry : exchangeSources.entrySet()) {
			try {
				Set<Currency> exchangeSourceCurrencies = exchangeSourceEntry.getValue().getCurrencyList(date);
				currencies.addAll(exchangeSourceCurrencies);
			} catch (ExchangeSourceIOException e) {
				log.error("Error whyle trying to get currencies list from bean: {}", exchangeSourceEntry.getKey());
				model.addAttribute("errorMsg", "Cant get currencies from " + exchangeSourceEntry.getValue().getExchangeSourceName());
			}
		}

		model.addAttribute("currencies", currencies);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public ConverterResponse calculate(@Valid ConverterForm form, BindingResult result, HttpServletResponse httpresponse) {
		ConverterResponse response = new ConverterResponse();

		if (result.hasErrors()) {
			response.setStatus(ResponseStatus.ERROR);
			List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
			List<FieldError> allErrors = result.getFieldErrors();
			for (FieldError fieldError : allErrors) {
				errorMesages.add(new ErrorMessage(fieldError.getField(), messageSourceHelper.getFieldErrorMessage(fieldError)));
			}
			response.setErrors(errorMesages);
			httpresponse.setStatus(HttpStatus.BAD_REQUEST.value());

		} else {
			response.setStatus(ResponseStatus.OK);
			calculateResult(form, response);

			if (!response.getErrors().isEmpty()) {
				response.setStatus(ResponseStatus.ERROR);
				httpresponse.setStatus(HttpStatus.BAD_REQUEST.value());
			}
		}

		return response;

	}

	private void calculateResult(ConverterForm form, ConverterResponse response) {
		StringBuilder sb = new StringBuilder();

		for (Entry<String, ExchangeSource> entry : exchangeSources.entrySet()) {
			try {
				Map<String, ExchangeRate> cources = entry.getValue().getExchangeCourseList(form.getDate());

				ExchangeRate courseFrom = cources.get(form.getCurrencyFrom());
				ExchangeRate courseTo = cources.get(form.getCurrencyTo());

				if (courseFrom == null || courseTo == null) {
					log.error("from = {}, to = {}", courseFrom, courseTo);
					throw new NoCourceException();
				}

				log.debug("amount: {}", form.getAmount());
				log.debug("courseFrom rate: {}", courseFrom.getRate());
				log.debug("courseTo rate: {}", courseTo.getRate());

				// amount * curr_from_rate / curr_to_rate

				BigDecimal res = form.getAmount().multiply(new BigDecimal(courseFrom.getRate()));
				res = res.divide(new BigDecimal(courseTo.getRate()), 10, RoundingMode.HALF_EVEN);
				res = res.setScale(3, RoundingMode.HALF_EVEN);

				sb.append(entry.getValue().getExchangeSourceName()).append(": ");
				sb.append(res).append(" ").append(courseTo.getCurrency());
				sb.append("</br>");

			} catch (ExchangeSourceIOException e) {
				response.getErrors()
				.add(new ErrorMessage(null, "Cant get exchange rates from " + entry.getValue().getExchangeSourceName()));
			} catch (NoCourceException e) {
				String msg = messageSourceHelper.getMessage("exception.noSourceException", new Object[] { entry.getValue()
						.getExchangeSourceName() });
				response.getErrors().add(new ErrorMessage(null, msg));
			}
		}

		response.setResult(sb.toString());
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(new ConverterForm());
		attachCurrenciesToModel(model);
		return "index";
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(new ConverterFormValidator());
	}

	@PostConstruct
	public void postConstruct() {
		exchangeSources = applicationContext.getBeansOfType(ExchangeSource.class);
		log.debug("currencySource beans: {}", exchangeSources);
	}
}
