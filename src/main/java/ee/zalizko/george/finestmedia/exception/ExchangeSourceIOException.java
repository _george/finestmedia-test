package ee.zalizko.george.finestmedia.exception;

/**
 * @author Georgii Zalizko
 * 
 */
public class ExchangeSourceIOException extends Exception {
	private static final long serialVersionUID = 1L;
}
