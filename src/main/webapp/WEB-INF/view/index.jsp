<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<meta charset="utf-8">
<title>Currency converterr</title>

<link rel="stylesheet" type="text/css" href="<c:url value="resources/css/pepper-grinder/jquery-ui-1.10.4.custom.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/stylesheet.css" />" />

<script src="<c:url value="resources/js/jquery-1.10.2.js" />"></script>
<script src="<c:url value="resources/js/jquery-ui-1.10.4.custom.min.js" />"></script>
<script src="<c:url value="resources/js/jquery.form.js" />"></script>
<script src="<c:url value="resources/js/javascript.js" />"></script>

</head>
<body>
	<h1>Currency converter</h1>
	<form:form commandName="converterForm" method="POST">

		<div class="error-block" id="error-block">
			<c:if test="${errorMsg ne null}">${errorMsg}</c:if>
		</div>

		<div class="field">
			<label for="amount">Amount:</label>
			<form:input path="amount" name="amount" id="amount" value="" />
		</div>

		<div class="field">
			<label for="currencyFrom">Currency from:</label>
			<form:select path="currencyFrom" name="currencyFrom" id="currencyFrom">
				<form:option value=""></form:option>
				<c:forEach items="${currencies}" var="currency">
					<form:option value="${currency.code}">${currency.code} - ${currency.name}</form:option>
				</c:forEach>
			</form:select>
		</div>

		<br />
		<br />

		<div class="field">
			<label for="currencyTo">Currency to:</label>
			<form:select path="currencyTo" name="currencyTo" id="currencyTo">
				<form:option value=""></form:option>
				<c:forEach items="${currencies}" var="currency">
					<form:option value="${currency.code}">${currency.code} - ${currency.name}</form:option>
				</c:forEach>
			</form:select>
		</div>

		<div class="field">
			<label for="date">Date:</label>
			<form:input path="date" type="text" name="date" id="date" cssClass="datepicker" value="" />
		</div>

		<br />
		<br />

		<input type="submit" value="Calculate" />

		<div id="result"></div>

	</form:form>
</body>
</html>
