# Finestmedia OÜ

## Java proovitöö kirjeldus

## AJAX valuutakalkulaator


Ülesande eesmärgiks on koostada valuutakalkulaator, mis võimaldab avalikult saada olevate kursitabelite alusel konverteerida määratud kuupäeval kehtivate kursside alusel erinevaid valuutasid.

Lehe avamisel tõmmatakse Eesti Panga ja Leedu Panga kodulehekülgedelt kasutatavate valuutade loetelu saamiseks eelmise päeva kursitabel (sama päeva kursitabelit lehel ei pakuta enne kella 13-t). 
 
Seejärel saab kasutaja sisestada:

* lähtesumma
* lähtevaluuta
    * select box, sisuks valuuta lühend + valuuta täisnimi, täisnime järgi tähestikulises järjekorras
* sihtvaluuta
    * sama mis eelmine
* kursi kuupäeva
    * input väli formaadis dd.mm.yyyy koos mõne javascript date pickeriga

Rakendus valideerib sisestatud väärtused ning kuvab vea korral adekvaatset veateadet vigase sisendi kohta. Kui sisestatud väärtused on korrektsed, tirib rakendus endale küsitud päeva kursitabelid, teeb soovitud arvutused ning kuvab kõigi kursitabelite alusel saadud tulemused eraldi. Kursitabeli laadimise ebaõnnestumisel või valitud valuuta puudumisel konkreetsest feedist peaks kasutajale kuvatama adekvaatset veateadet.

### Nõuded
- Lahendus peab kasutama Objekt Orienteeritud lahenemist
- Lahendus peab kasutama MVC stiilis ulesehitust
- Rakendus teha Apache Tomcat-il.
- Javascript osad voiksid kasutama monda laialdaselt levinud raamistiku (soovitatavalt jQuery)
- Lahendus peab olema stiilitud CSS-i abiga, st kindlasti ei sobi uhes reas 5 vormi elementi. Eesmark ei ole moota kujundaja oskusi vaid pohimottelist kasutajaliideste ehitamist ning CSS-i oskusi. Soovikorral voib erinevate effektide (gradientid, ummargused nurgad, text shadowd jms) jaoks kasutada ka CSS3 voimalusi.
- Kasutajale esitatav valuutade nimekiri peaks olema koondnimekiri erinevate allikate pakutavatest valuutadest, mis genereeritakse dunaamiliselt
- Tulevikus kursitabelite allikate lisamine voi muutmine peaks toimuma voimalikult lihtsalt, naiteks uue konfiguratsioonibloki lisamisena ja/voi subclassi loomisega
- Kursitabelite vaartused tuleks rakenduses cacheda, st. rakendus ei tohiks uhelegi allikale uhe kindla kuupaeva kohta teha kunagi rohkem kui uhe paringu
- Valuutaarvutused peaksid toimuma serveri poolel ning tulemused (voi veaolukorrad ) saadakse AJAX paringuga, soovitatavalt struktureeritult (JSON, XML...)

### Kursitabelite XML feedid
- Eesti Pank - [Eesti Pank link][]
- Leedu Pank - [Leedu Pank link][]

Alates 1. jaanuarist 2011 on Eestis krooni asemel kasutusel euro ja Eesti Pank on lõpetanud päevakursside fikseerimise ning proovitöö lahendamisel võib kasutada Eesti Panga viimaseid andmeid.

** Lahenduseks saata lähtekood koos juhendiga käivitamiseks. **

[Eesti Pank link]: http://statistika.eestipank.ee/Reports?type=curd&format=xml&date1=2010-12-30&lng=est&print=off
[Leedu Pank link]: http://webservices.lb.lt/ExchangeRates/ExchangeRates.asmx/getExchangeRatesByDate?Date=2010-12-30